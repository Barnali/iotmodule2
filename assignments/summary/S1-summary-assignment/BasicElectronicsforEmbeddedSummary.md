![sensors](/uploads/bcf006f9975de517b1516c9fc9da2360/sensors.png)

A better term for a sensor is a transducer.

A transducer is any physical device that converts one form of energy into another.

Sensor is, the transducer converts some physical energy into an electrical signal that can then be used to take a reading.

For Example : A microphone is a sensor that takes vibrational energy (sound waves), and converts it to electrical signal for the system to relate it back to the original sound.

![top_sensor_types_used_in_iot-03](/uploads/9ac7310e8834a24c20b5b0670684d5b6/top_sensor_types_used_in_iot-03.png)

# Actuators

Another type of transducer is an actuator.

An actuator operates in the **reverse direction** of a sensor.

Actuator takes an electrical input and turns it into physical action.

For example: an electric motor, a hydraulic system, and a pneumatic system are all different types of actuators.

![Transducers](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-05.png)

# Analog Vs Digital comparison

| Parameters | Analog | Digital |
|------------|--------|---------|
| Signal | Analog signal is a continuous signal which represents physical measurements. | Digital signals are discrete time signals generated by digital modulation. |
| Waves | Denoted by sine waves | Denoted by square waves |
| Representation | Uses continuous range of values to represent information | Uses discrete or discontinuous values to represent information |
| Example | Human voice in air, analog electronic devices. | Computers, CDs, DVDs, and other digital electronic devices. |
| Technology | Analog technology records waveforms as they are. | Samples analog waveforms into a limited set of numbers and records them. |
| Applications | Thermometer | PCs, PDAs |


![analogvsdigi](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/analogvsdigital.jpg)

# Micro-controllers Vs Micro-processors

![image](/uploads/bc368f775c86d17dea18af666582ef94/image.png)


# Introduction to RPI

![image](/uploads/47569364482fd3bef16363790c3134e8/image.png)

Raspberry Pi is a

* Mini Computer
* Limited but large power for its size
* No storage
* It is a SOC (System On Chip)
* We can connect shields (Shields - addon functionalities)
* Can connect multiple Pi’s together
* Microprocessor
* Can load a linux OS on it
* Pi uses ARM
* Connect to sensors or actuators


Raspberry Pi Interfaces :

1. GPIO
1. UART
1. SPI
1. I2C
1. PWM

# Serial and Parallel Communication

![serial](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/10.png)

## Parallel Interfaces

1. GPIO

## Serial Interfaces

1. UART
1. SPI
1. I2C