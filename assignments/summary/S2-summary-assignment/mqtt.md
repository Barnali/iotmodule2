### **MQTT (Message Queuing Telemetry Transport)**

* MQTT is lightweight, **publish-subscribe** network protocol that transports messages between devices.
* MQTT Client as a publisher sends a message to the **MQTT broker** whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
* **MQTT topics** are a form of addressing that allows MQTT clients to share information (publish the messages).

![MQTT](https://user-content.gitlab-static.net/3f94c296b833902f9b28a1d44e807af3a64d42e0/68747470733a2f2f696f747261737069322e66696c65732e776f726470726573732e636f6d2f323031362f30322f7075622d7375622d6d7174742d31303234783538382e706e673f773d373030)