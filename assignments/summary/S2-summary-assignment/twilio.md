# Twilio

Twilio (/ˈtwɪlioʊ/) is an American cloud communications platform as a service (CPaaS) company based in San Francisco, California. Twilio allows software developers to programmatically make and receive phone calls, send and receive text messages, and perform other communication functions using its web service APIs.

![image](/uploads/78e1ca073b8feb4e554e687f12cee226/image.png)

![image](/uploads/d862814e4cee217aaeb944eec1bb8ef4/image.png)


# Working

Twilio receives the message from the carrier over a dedicated connection between Twilio and that carrier. The message then lands in Twilio's messaging processing stack where Twilio has written software to receive and interpret that message. ... Twilio will then make a request to the application.

![image](/uploads/793bc69592d7f6f68796007c489e51d8/image.png)