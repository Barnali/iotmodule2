## **MODBUS Communication Protocol**

* `Modbus` is a communication protocol used for transmitting information over serial lines between electronic devices.
* In a standard Modbus network, there is one Master and up to 247 Slaves, each with a unique Slave Address from 1 to 247.
* Modbus is used in multiple client-server applications to monitor and program devices, to communicate between intelligent devices and sensors and instruments.

![](https://user-content.gitlab-static.net/fd9568e610647575d9ff0a8dd9d0764ac1eb16cd/68747470733a2f2f7468656175746f6d697a6174696f6e2e636f6d2f77702d636f6e74656e742f75706c6f6164732f323031372f31302f4d6f646275732d506c616e742e706e67)

* Modbus protocol can be used over 2 interfaces
  * RS485 - called as Modbus RTU
  * Ethernet - called as Modbus TCP/IP

#### **RS 485 - MODBUS RTU**

* RS485 is a serial data transmission standard widely used in industrial implementations.
* The MODBUS RS485 protocol defines communication between a host (master) and devices (slaves) that allows querying of device configuration and monitoring.

![RS 485](https://user-content.gitlab-static.net/e03e89af1836d7476d1f633d60667212140c91fd/68747470733a2f2f7777772e636f6e74726f6c676c6f62616c2e636f6d2f6173736574732f55706c6f6164732f313831322d46656174332d466967312d3635302d636f6d70726573736f722e706e67)

#### **Ethernet - MODBUS TCP/IP**

* Modbus TCP/IP combines a physical network (Ethernet), with a networking standard (TCP/IP).
* Ethernet Basics TCP/IP (Transmission Control Protocol/Internet Protocol) is a set of protocols independent of the physical medium used to transmit data, but most data transmission for Internet communication begins and ends with Ethernet frames.

![](https://user-content.gitlab-static.net/f0f6bee7fd374f79a9ae54f420f03376ca93887e/68747470733a2f2f7777772e666c6561706c632e69742f696d616765732f41727469636f6c692f53374f70656e4d6f646275732f6d6f646275732d67726170682d656e2d7a6f6f6d2e6a7067)